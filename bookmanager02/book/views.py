from django.http import HttpResponse
from django.shortcuts import render


# Create your views here.


def index(request):
    # 在这里实现增删改查
    return HttpResponse('index')


################## 新增数据 #######################
from book.models import BookInfo

# 方式1
book = BookInfo(
    name='Django',
    pub_date='2000-1-1',
    read_count=10
)
book.save()
# 方式1必须调用name.save()才能保存到数据库

# 方式2
# objects
'''
objects ---- 相当于一个代理
'''
BookInfo.objects.create(
    name='测试开发入门',
    pub_date='2020-1-1',
    read_count=100
)

################## 修改数据 #######################

# 方式1
book = BookInfo.objects.get(id=6)
book.name = "运维开发入门"
book.save()
# 想要保存数据也需要调用对象的save方法

# 方式2
# filter 过滤
BookInfo.objects.filter(id=6).update(name='爬虫入门', comment_count=666)

################## 删除数据 #######################

# 删除分2种，物理删除(这条记录的数据删除) 和逻辑删除(修改标记) 例如 is_delete = False
book = BookInfo.objects.get(id=6)
# 方式1
book.delete()
#
# 方式2
BookInfo.objects.get(id=6).delete()
BookInfo.objects.filter(id=5).delete()

#################### 查询数据 #######################
'''
1. get 查询单一结果，不存在则会抛出异常
2. all 查询所有
3. 查询结果数量
'''

# 1
BookInfo.objects.get(id=1)
# 查询多个
from book.models import PeopleInfo

PeopleInfo.objects.all()
# 查询结果数量
PeopleInfo.objects.count()

###############过滤查询#####################
# 实现SQL中where功能，包括
'''
1. filter：获取n个结果(得到列表)   n=0,n=1,...
2. exclude：排除掉n个结果   n=0,n=1,...
3. get(得到一个)
语法形式：模型类名.objects.filter(属性名__运算符=值)
'''

'''

'''
# 查询编号为1的图书
BookInfo.objects.get(id=1)
BookInfo.objects.get(id__exact=1)
BookInfo.objects.get(pk=1)
# 查询书名包含'湖'的图书
BookInfo.objects.filter(name__contains='湖')
# 查询书名以'部'结尾的图书
BookInfo.objects.filter(name__endswith='部')
# 查询书名为空的图书
BookInfo.objects.filter(name__isnull=True)
# 查询编号为1或3或5的图书
BookInfo.objects.filter(id__in=[1, 3, 5])
# 查询编号大于3的图书
'''
大于：gt
大于等于：gte

小于：lt
小于等于：lte
'''
BookInfo.objects.filter(id__gt=3)
# 查询编号不等于3的书籍
BookInfo.objects.exclude(id=3)
# 查询1980年发表的图书
BookInfo.objects.filter(pub_date__year=1980)
# 查询1990年1月1日后发表的图书
BookInfo.objects.filter(pub_date__gt='1990-01-01')
BookInfo.objects.filter(pub_date__gt='1990-1-1')

########################F对象###########################

from django.db.models import F

# 使用：两个属性的比较
# 语法形式： 以filter 为例：模型类名.objects.filter(属性名__运算符=F('第二个属性名'))
BookInfo.objects.filter(read_count__gte=F('comment_count'))

##############Q对象###################
# 并且查询
# 查询阅读量大于20，并且编号小于3的图书
BookInfo.objects.filter(read_count__gt=20).filter(id__lt=3)
# 等价
BookInfo.objects.filter(read_count__gt=20, id__lt=3)

from django.db.models import Q

# 或者方法：模型类名.objects.filter(Q(属性名__运算符=值)|Q(属性名__运算符=值)|...)
# 并且方法：模型类名.objects.filter(Q(属性名__运算符=值)&Q(属性名__运算符=值)&...)
# not 非语法： 模型类名.objects.filter(~Q(属性名__运算符=值))
BookInfo.objects.filter(Q(read_count__gt=20) | Q(id__lt=3))
BookInfo.objects.filter(~Q(id=3))





###########聚合函数###########
from django.db.models import Sum,Max,Min,Avg,Count

# 模型类名.objects.aggregate(Xxx('字段名'))
BookInfo.objects.aggregate(Sum('read_count'))

#############排序#######################
BookInfo.objects.all().order_by('-read_count')



####################级联操作######################


# 查询书籍为1的所有人物信息

book = BookInfo.objects.get(id=1)
book.peopleinfo_set.all()


# 查询人物为1的书籍信息
person = PeopleInfo.objects.get(id=1)
# 直接调用对象.属性即可
person.book.name
person.book.read_count


############关联过滤查询###############

#  语法形式
# 查询1的数据，条件为n
# 模型类名.objects.(关联模型类型(小写)__字段名__运算符=值)


# 查询图书，要求图书人物为‘郭靖’
BookInfo.objects.filter(peopleinfo__name__exact='郭靖')
BookInfo.objects.filter(peopleinfo__name='郭靖')
# 查询图书，要求图书中人物的描述包含“八”
BookInfo.objects.filter(peopleinfo__description__contains='八')

# 查询书名为天龙八部的所有人物
PeopleInfo.objects.filter(book__name='天龙八部')
PeopleInfo.objects.filter(book__name__exact='天龙八部')
# 查询图书阅读量大于30的所有人物
PeopleInfo.objects.filter(book__read_count__gt=30)

















