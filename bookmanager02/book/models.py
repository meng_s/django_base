from django.db import models

# Create your models here.
'''
2. 定义属性：
    2.1 属性名=models.类型(选项)
    2.2 不要实用关键字，不要使用连续的下划线
    2.3 类型：mysql的类型
    2.4 是否有默认值，是否唯一，是否为null
        CharField 必须设置 max_length
creat table 'qq_user'(
    id int,
    name varchar(10) not null default ''
)
3. 改变表的名称
    默认表的名称是子应用名_类名
    修改表的名字
'''


class BookInfo(models.Model):
    name = models.CharField(max_length=50, unique=True)
    pub_date = models.DateField(null=True)
    read_count = models.IntegerField(default=0)
    comment_count = models.IntegerField(default=0)
    is_delete = models.BooleanField(default=False)
    # 系统自动添加一对多关系模型 ,关联模型(小写)_set
    # peopleinfo_set = [peopleinfo,peopleinfo,...]

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'bookInfo'  # 修改表的名字
        verbose_name = '书籍管理'  # admin站点使用的名字


class PeopleInfo(models.Model):
    GENDER_CHOICE = (
        (1, 'male'),
        (2, 'female'),
    )
    name = models.CharField(max_length=20, unique=True)
    gender = models.SmallIntegerField(choices=GENDER_CHOICE, default=1)
    description = models.CharField(max_length=100, null=True)
    is_delete = models.BooleanField(default=False)

    # 外键：系统会自动为外键添加_id
    book = models.ForeignKey(BookInfo, on_delete=models.CASCADE)
    # book=BookInfo()
    def __str__(self):
        return self.name

    class Meta:
        db_table = 'peopleInfo'
        verbose_name = '人物信息'
