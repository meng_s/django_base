from django.shortcuts import render

# Create your views here.
'''
视图：python函数
视图函数要求：
  1. 视图函数的第一个参数就是接收请求
  2.  必须返回一个响应
'''
from django.http import HttpRequest
from django.http import HttpResponse


# 希望用户输入http://127.0.0.1/index/访问主页
# 来访问视图函数


def index(request):
    # return HttpResponse('ok')
    # render()：渲染模板
    # request,template,context=none
    """
    request: 请求
    template:模板名字
    context=none
    """

    # 模拟数据处理
    context = {
        'name': '马上双十一',
    }
    return render(request, 'book/index.html', context=context)
