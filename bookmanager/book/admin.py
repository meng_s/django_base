from book.models import BookInfo, PeopleInfo
from django.contrib import admin

# Register your models here.
# 注册模型表
admin.site.register(BookInfo)
admin.site.register(PeopleInfo)
