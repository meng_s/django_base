from book.views import create_book, shop, register, Json, method, response, set_cookie, get_cookie, set_session, \
    get_session, login, LoginView, OrderView
from django.urls import path, converters
from django.urls.converters import register_converter


# 3. 使用
# 1. 定义转换器
class MobileConverter:
    # 验证数据的关键是正则
    regex = '1[3-9]\d{9}'

    # 验证没有问题的数据给视图函数
    def to_python(self, value):
        return int(value)
    # def to_url(self, value):
    #     return str(value)


# 2. 注册转换器
register_converter(MobileConverter, 'phone')

urlpatterns = [
    path('create/', create_book),
    # <转换器名字:变量名>
    # 本质是对变量数据进行正则验证
    path('<int:city_id>/<phone:shop_id>', shop),
    path('register/', register),
    path('json/', Json),
    path('method/', method),
    path('res/', response),
    path('set_cookie/', set_cookie),
    path('get_cookie/', get_cookie),
    path('set_session/', set_session),
    path('get_session/', get_session),
    path('login/', login),
    path('LoginView/',LoginView.as_view()),
    path('order/', OrderView.as_view())
]