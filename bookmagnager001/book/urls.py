from book.views import index
from django.urls import path

urlpatterns = [
    path('book/', index),
    path('book/<view_id>/<iid>', index)

]
